﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using CorrectionTP.Models;
using CorrectionTP.Views;

namespace CorrectionTP.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class ItemsPage : ContentPage
    {
        //ItemsViewModel viewModel;

        public ItemsPage()
        {
            InitializeComponent();
        }

        public ItemsPage(int CategoryId) : this()
        {
            Title = HomeMenuItem.GetMenuItems().Find(c => c.Id == CategoryId).Title;
            List<Item> products = Item.GetProducts().Where(p => p.CategoryId == CategoryId).ToList();
            ListeProductsView.ItemsSource = products;
            ListeProductsView.ItemSelected += (sender, e) =>
            {
                Item product = e.SelectedItem as Item;
                Navigation.PushAsync(new ItemDetailPage(product.Id));
            };
        }

        
    }
}