﻿using System;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using CorrectionTP.Models;


namespace CorrectionTP.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
        }

        public ItemDetailPage(int ProductId) : this()
        {
            BindingContext = Item.GetProducts().Find(p => p.Id == ProductId);
        }

        private void AddToCartClick(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Panier(BindingContext as Item));
        }
    }
}