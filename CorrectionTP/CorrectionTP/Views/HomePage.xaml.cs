﻿using CorrectionTP.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CorrectionTP.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();
            GenerateHomeProduct();
        }

        private void GenerateHomeProduct()
        {
            List<Item> products = new List<Item>();
            products.Add(Item.GetProducts()[0]);
            products.Add(Item.GetProducts()[1]);
            int col = 0;
            foreach(Item p in products)
            {
                StackLayout s = new StackLayout() { };
                Image img = new Image { Source = ImageSource.FromFile(p.UrlImage), Aspect=Aspect.AspectFit };
                Label l = new Label { Text = p.Text, HorizontalTextAlignment=TextAlignment.Center };
                s.Children.Add(img);
                s.Children.Add(l);
                HomeProduct.Children.Add(s);
                //Gesture Tap
                TapGestureRecognizer tap = new TapGestureRecognizer();
                tap.Tapped += (sender, e) =>
                {
                    Navigation.PushAsync(new ItemDetailPage(p.Id));
                };
                s.GestureRecognizers.Add(tap);
                Grid.SetRow(s, 0);
                Grid.SetColumn(s, col);
                col++;
            }

        }
    }
}