﻿using CorrectionTP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CorrectionTP.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Panier : ContentPage
    {
        public Panier()
        {
            InitializeComponent();
        }

        public Panier(Item _product) : this()
        {
            PanierModel.AddToCart(_product);
            GeneratePanier();
            TotalLabel.Text = "Total : " + PanierModel.LePanier.Total + " €";
        }

        private void GeneratePanier()
        {
            foreach(PanierItem p in PanierModel.LePanier.Products)
            {
                StackLayout s = new StackLayout() { Orientation = StackOrientation.Horizontal};
                Label titre = new Label { Text = p.Item.Text };
                Label qty = new Label { Text = p.Qty.ToString() };
                Label prix = new Label { Text = p.Item.Price.ToString() + "€" };
                Label prix_total = new Label { Text = (p.Qty * p.Item.Price).ToString() + "€" };
                s.Children.Add(titre);
                s.Children.Add(qty);
                s.Children.Add(prix);
                s.Children.Add(prix_total);
                PanierLayout.Children.Add(s);
            }

        }
    }
}