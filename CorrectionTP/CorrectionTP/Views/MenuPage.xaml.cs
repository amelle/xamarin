﻿using CorrectionTP.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CorrectionTP.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MenuPage : ContentPage
    {
        List<HomeMenuItem> menuItems;
        public ListView listeView;
        public MenuPage()
        {
            InitializeComponent();

            menuItems = HomeMenuItem.GetMenuItems();

            listeView = ListViewMenu;
            ListViewMenu.ItemsSource = menuItems;            
        }
    }
}