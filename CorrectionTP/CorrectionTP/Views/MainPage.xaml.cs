﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using CorrectionTP.Models;

namespace CorrectionTP.Views
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();
            MasterPage.listeView.ItemSelected += Menu_Selected;
        }


        private void Menu_Selected(object sender, SelectedItemChangedEventArgs e)
        {
            
            if (e.SelectedItem == null)
                return;

            var id = (int)((HomeMenuItem)e.SelectedItem).Id;
            if (id == 1)
            {
                Detail = new NavigationPage(new HomePage());
            }
            else
            {
                Detail = new NavigationPage(new ItemsPage(id));
            }

            IsPresented = false;

            MasterPage.listeView.SelectedItem = null;
        }
    }
}