﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CorrectionTP.Models
{
    public class PanierModel
    {
        private decimal total;

        private List<PanierItem> products;

        public decimal Total { get => total; }
        public List<PanierItem> Products { get => products; set => products = value; }

        public PanierModel()
        {
            Products = new List<PanierItem>();
        }
        public static PanierModel LePanier = new PanierModel();

        public void CalculeTotal()
        {
            total = 0;
            foreach(PanierItem p in Products)
            {
                total += p.Qty * p.Item.Price;
            }
        }

        public static void AddToCart(Item _product)
        {
            bool found = false;
            foreach (PanierItem p in PanierModel.LePanier.Products)
            {
                if (p.Item.Id == _product.Id)
                {
                    p.Qty++;
                    found = true;
                    break;
                }
            }
            if (!found)
            {
                PanierModel.LePanier.Products.Add(new PanierItem() { Item = _product });
            }
            LePanier.CalculeTotal();
        }
    }


    public class PanierItem
    {
        private Item item;
        private int qty;

        public Item Item { get => item; set => item = value; }
        public int Qty { get => qty; set => qty = value; }

        public PanierItem()
        {
            Qty = 1;
        }
    }
}
