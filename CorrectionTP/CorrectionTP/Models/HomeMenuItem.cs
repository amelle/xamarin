﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CorrectionTP.Models
{
    
    public class HomeMenuItem
    {
        public int Id { get; set; }

        public string Title { get; set; }


        public static List<HomeMenuItem> GetMenuItems()
        {
            return new List<HomeMenuItem>()
            {
                new HomeMenuItem {Id = 1, Title="Home" },
                new HomeMenuItem {Id = 2, Title="Categorie 1" },
                new HomeMenuItem {Id = 3, Title="Categorie 2" },
                new HomeMenuItem {Id = 4, Title="Categorie 3" },
            };
        }

    }
}
