﻿using System;
using System.Collections.Generic;

namespace CorrectionTP.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }

        public string UrlImage { get; set; }

        public decimal Price { get; set; }

        public int CategoryId { get; set; }

        public static List<Item> GetProducts()
        {
            List<Item> Products = new List<Item>()
            {
                new Item
                {
                    Id = 1,
                    Text = "Produit 1",
                    CategoryId = 2,
                    Description = "Description produit 1",
                    Price = 1000,
                    UrlImage = "test.jpg"
                },
                new Item
                {
                    Id = 2,
                    Text = "Produit 2",
                    CategoryId = 2,
                    Description = "Description produit 2",
                    Price = 1000,
                    UrlImage = "test.jpg"
                },
                new Item
                {
                    Id = 3,
                    Text = "Produit 3",
                    CategoryId = 4,
                    Description = "Description produit 3",
                    Price = 1000,
                    UrlImage = "test.jpg"
                },
                new Item
                {
                    Id = 4,
                    Text = "Produit 4",
                    CategoryId = 3,
                    Description = "Description produit 4",
                    Price = 1000,
                    UrlImage = "test.jpg"
                },
                new Item
                {
                    Id = 1,
                    Text = "Produit 1",
                    CategoryId = 1,
                    Description = "Description produit 1",
                    Price = 1000,
                    UrlImage = "test.jpg"
                },
            };

            return Products;
        }
    }
}