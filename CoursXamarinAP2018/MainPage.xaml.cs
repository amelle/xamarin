﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CoursXamarinAP2018
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            //firstStack.Children.Add(new Button { Text = "Second button" });
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            //DisplayAlert("MessageBox", "Bonjour ","OK");
            Navigation.PushAsync(new MorpionPage(joueur1.Text, joueur2.Text));
        }

        private void FormPersonne_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new FormPersonne());
        }
    }
}
