﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CoursXamarinAP2018
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListContact : ContentPage
    {
        private ObservableCollection<Contact> listeContacts;
        public ListContact()
        {
            InitializeComponent();
            listeContacts = new ObservableCollection<Contact>(Contact.contacts);
            listeViewContact.ItemsSource = listeContacts;
            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if(!AuthService.IsLogged)
            {
                Navigation.RemovePage(this);
                Navigation.PushAsync(new Login());
                
            }
        }


        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        public ListContact(Contact contact) : this()
        {
            Contact.contacts.Add(contact);
            listeContacts.Add(contact);
        }

        //private void DeleteButton(object sender, EventArgs e)
        //{
        //    Contact deleteContact = listeViewContact.SelectedItem as Contact;
        //    if(deleteContact == null)
        //    {
        //        DisplayAlert("Erreur", "Merci de selectionner le contact à supprimer", "OK");
        //    }
        //    else
        //    {
        //        Contact.contacts.Remove(deleteContact);
        //        listeContacts.Remove(deleteContact);
        //    }
        //}

        //private void EditButton(object sender, EventArgs e)
        //{
        //    Contact editContact = listeViewContact.SelectedItem as Contact;
        //    if(editContact == null)
        //    {
        //        DisplayAlert("Erreur", "Merci de selectionner le contact à supprimer", "OK");
        //    }
        //    else
        //    {
        //        Navigation.PushAsync(new FormContact(editContact));
        //    }
        //}

        private void DeleteButton(object sender, EventArgs e)
        {
            MenuItem m = sender as MenuItem;
            Contact deleteContact = m.CommandParameter as Contact;
            Contact.contacts.Remove(deleteContact);
            listeContacts.Remove(deleteContact);
        }
        private void EditButton(object sender, EventArgs e)
        {
            MenuItem m = sender as MenuItem;
            Contact editContact = m.CommandParameter as Contact;
            Navigation.PushAsync(new FormContact(editContact));
        }

        private void SearchContact(object sender, EventArgs e)
        {
            string search = (sender as SearchBar).Text;
            listeContacts = new ObservableCollection<Contact>(Contact.SearchContact(search));
            listeViewContact.ItemsSource = listeContacts;
        }
    }
}