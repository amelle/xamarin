﻿using System;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace CoursXamarinAP2018
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new Essential();
        }

        protected override void OnStart()
        {
            Debug.WriteLine("Start application");
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            Debug.WriteLine("Sleep app");
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            Debug.WriteLine("Resume app");
            // Handle when your app resumes
        }
    }
}
