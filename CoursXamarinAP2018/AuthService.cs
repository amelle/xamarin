﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoursXamarinAP2018
{
    public class AuthService
    {
        public static bool IsLogged = false;

        public static List<UserApp> users = new List<UserApp>() { new UserApp { Email = "ihab@utopios.net", Password = "123456" } };

        public static bool Login(string email, string password)
        {
            IsLogged = users.FirstOrDefault((u) => u.Email == email && u.Password == password) != null;
            return IsLogged;
        }
    }

    public class UserApp
    {
        private string email;
        private string password;

        public string Email { get => email; set => email = value; }
        public string Password { get => password; set => password = value; }
    }
}
