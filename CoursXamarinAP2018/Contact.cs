﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CoursXamarinAP2018
{
    public class Contact
    {
        private string nom;
        private string prenom;
        private string telephone;

        public string Nom { get => nom; set => nom = value; }
        public string Prenom { get => prenom; set => prenom = value; }
        public string Telephone { get => telephone; set => telephone = value; }


        public static List<Contact> contacts = new List<Contact>();

        public void Update()
        {
            contacts.ForEach((c) =>
            {
                if(c.Telephone == Telephone)
                {
                    c.Nom = Nom;
                    c.Prenom = Prenom;
                }
            });
        }

        public static List<Contact> SearchContact(string search)
        {
            return contacts.Where(c => (c.Nom.Contains(search) || c.Prenom.Contains(search) || c.Telephone.Contains(search))).ToList();
        }
    }
}
