﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CoursXamarinAP2018
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FormPersonne : ContentPage
    {
        private ObservableCollection<Personne> listePersonne;
        public FormPersonne()
        {
            InitializeComponent();
            listePersonne = new ObservableCollection<Personne>();
            listePersonneView.ItemsSource = listePersonne;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            Personne p = new Personne
            {
                Nom = entry_nom.Text,
                Prenom = entry_prenom.Text
            };
            listePersonne.Add(p);
        }
    }
}