﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CoursXamarinAP2018
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MorpionPage : ContentPage
    {
        private bool joueur1 = true;
        public MorpionPage()
        {
            InitializeComponent();
            GenerateButton();
        }

        public MorpionPage(string j1, string j2) : this()
        {
            label_joueur_1.Text = j1;
            label_joueur_2.Text = j2;
        }


        private void GenerateButton()
        {
            for(int i = 1; i <= 3; i++)
            {
                morpion.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
                morpion.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            }
            for(int i=0; i< 3; i++)
            {
                for(int j=0; j < 3; j++)
                {
                    Button b = new Button();
                    morpion.Children.Add(b);
                    b.Clicked += ClickButton;
                    Grid.SetRow(b, i);
                    Grid.SetColumn(b, j);
                }
            }
        }

        private void ClickButton(object sender, EventArgs e)
        {
            if((sender as Button).Text == null)
            {
                (sender as Button).Text = (joueur1) ? "X" : "O";
                TestWin();
                joueur1 = !joueur1;
            }
        }

        private void TestWin()
        {
            bool win = false;
            List<Button> liste;
            //Verification sur les lignes 
            for (int x = 0; x < 3; x++)
            {
                liste = morpion.Children.Where(element => 
                ((element as Button).Text == ((joueur1) ? "X" : "O") && Grid.GetRow(element) == x)
                ).Cast<Button>().ToList();
                if(liste.Count == 3)
                {
                    win = true;
                }
            }
            //Verification sur les columns
            for (int x = 0; x < 3; x++)
            {
                liste = morpion.Children.Where(element =>
                ((element as Button).Text == ((joueur1) ? "X" : "O") && Grid.GetColumn(element) == x)
                ).Cast<Button>().ToList();
                if (liste.Count == 3)
                {
                    win = true;
                }
            }
            //Verification sur la premiere diagonale
            liste = morpion.Children.Where(element =>
                ((element as Button).Text == ((joueur1) ? "X" : "O") && Grid.GetRow(element) == Grid.GetColumn(element))
                ).Cast<Button>().ToList();

            if (liste.Count == 3)
            {
                win = true;
            }
            //Verification sur la deuxieme diagonale
            liste = morpion.Children.Where(element =>
                ((element as Button).Text == ((joueur1) ? "X" : "O") && (Grid.GetRow(element)+1) + (Grid.GetColumn(element)+1) == 4)
                ).Cast<Button>().ToList();

            if (liste.Count == 3)
            {
                win = true;
            }

            if (win)
            {
                DisplayAlert("Bravo", "Vous avez gagné", "Ok");
            }
            //List<Button> liste = morpion.Children.Where(x => (x as Button).Text == ((joueur1) ? "X" : "O")).Cast<Button>().ToList();
            //foreach(Button b in liste)
            //{
            //    int x = Grid.GetRow(b);
            //    int y = Grid.GetColumn(b);
            //}
        }

        private void Back_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }
    }
}