﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CoursXamarinAP2018
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterDetailPage1Master : ContentPage
    {
        public ListView ListView;
        public ObservableCollection<MasterMenuItem> MenuItems { get; set; }

        public MasterDetailPage1Master()
        {
            InitializeComponent();
            MenuItems = new ObservableCollection<MasterMenuItem>(){
                    new MasterMenuItem { Id = 0, Title = "Page 1", TargetType = typeof(MorpionPage) },
                    new MasterMenuItem { Id = 1, Title = "Page 2" },
                    new MasterMenuItem { Id = 2, Title = "Page 3" },
                    new MasterMenuItem { Id = 3, Title = "Page 4" },
                    new MasterMenuItem { Id = 4, Title = "Page 5" },
                };
            ListView = MenuItemsListView;
            MenuItemsListView.ItemsSource = MenuItems;
        }
    }
}