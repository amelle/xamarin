﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CoursXamarinAP2018
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Essential : ContentPage
    {
        SensorSpeed speed = SensorSpeed.UI;
        public Essential()
        {
            InitializeComponent();
            Accelerometer.Start(speed);

            Accelerometer.ReadingChanged += Change_Accelerometer;
            //GetLocationAsync();
        }

        private async void GetLocationAsync()
        {
            var request = new GeolocationRequest(GeolocationAccuracy.Medium);
            var location = await Geolocation.GetLocationAsync(request);
            //texte.Text = $"Latitude {location.Latitude} Longitude : {location.Longitude}";
        }

        private void Change_Accelerometer(object sender, AccelerometerChangedEventArgs e)
        {
            AccelerometerData data = e.Reading;
            if(box.Y >= box.ParentView.Y && box.Y <= box.ParentView.Height)
            {
                box.Margin = new Thickness()
                {
                    Top = box.Margin.Top + (data.Acceleration.Y * 5),
                    Left = box.Margin.Left,
                };
            }
            else
            {
                box.Margin = new Thickness()
                {
                    Top = 0,
                    Left = box.Margin.Left,
                };
            }

            if(box.X >= box.ParentView.X && box.X <= box.ParentView.Width)
            {
                box.Margin = new Thickness()
                {
                    Left = box.Margin.Left - (data.Acceleration.X * 5),
                    Top = box.Margin.Top
                };
            }
            else
            {
                box.Margin = new Thickness()
                {
                    Left = 0,
                    Top = box.Margin.Top
                };
            }
            //texte.Margin = new Thickness() { Left = texte.Margin.Left + data.Acceleration.X * 10};
            //texte.Text = $"X : {data.Acceleration.X} Y : {data.Acceleration.Y} Z : {data.Acceleration.Z}";
        }
    }
}