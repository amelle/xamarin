﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CoursXamarinAP2018
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        public Login()
        {
            InitializeComponent();
            img.Source = ImageSource.FromUri(new Uri("https://www.domolandes.fr/wp-content/uploads/2016/10/cesi-567x420.jpg"));
        }

        private void LoginClick(object sender, EventArgs e)
        {
            if(AuthService.Login(entry_email.Text, entry_password.Text))
            {
                Navigation.RemovePage(this);
                Navigation.PushAsync(new ListContact());
            }
            else
            {
                DisplayAlert("Erreur", "Erreur email ou mot de passe", "OK");
            }
        }
    }
}