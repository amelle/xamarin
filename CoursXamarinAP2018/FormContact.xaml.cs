﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CoursXamarinAP2018
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FormContact : ContentPage
    {
        private Contact editContact;
        public FormContact()
        {
            InitializeComponent();
        }

        public FormContact(Contact c) : this()
        {
            editContact = c;
            text_nom.Text = c.Nom;
            text_prenom.Text = c.Prenom;
            text_telephone.Text = c.Telephone;
            text_telephone.IsEnabled = false;
        }

        private void Add_Button(object sender, EventArgs e)
        {
            if(editContact == null)
            {
                Contact c = new Contact
                {
                    Nom = text_nom.Text,
                    Prenom = text_prenom.Text,
                    Telephone = text_telephone.Text
                };

                text_nom.Text = null;
                text_prenom.Text = null;
                text_telephone.Text = null;
                Navigation.PushAsync(new ListContact(c));
            }
            else
            {
                editContact.Nom = text_nom.Text;
                editContact.Prenom = text_prenom.Text;
                editContact.Update();
                Navigation.PushAsync(new ListContact());
            }
            
        }
    }
}